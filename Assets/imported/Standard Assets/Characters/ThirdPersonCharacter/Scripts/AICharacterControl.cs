using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for

        public float range;
        public float rangedRange;

        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

            agent.updateRotation = false;
            agent.updatePosition = true;
        }


        private void Update()
        {
            if (target.GetComponent<Unit>() != null && target.GetComponent<Unit>().currentHealth <= 0)
            {
                agent.SetDestination(transform.position);
                character.Move(Vector3.zero, false, false);
                return;
             }
            if (target != null)
                agent.SetDestination(target.position);

            if (agent.remainingDistance > agent.stoppingDistance)
                character.Move(agent.desiredVelocity, false, false);
            else
                character.Move(Vector3.zero, false, false);

            if (IsInMeleeRangeOf( ) || (rangedRange > 0 && IsInRangedRangeOf()) )
            {
                RotateTowards(target);
            }
        }

        public bool IsInMeleeRangeOf( )
        {
            if (target.GetComponent<Unit>() != null && target.GetComponent<Unit>().currentHealth <= 0)
                return false;
            float distance = Vector3.Distance(transform.position, target.position);
            return distance < range;
        }
        public bool IsInRangedRangeOf()
        {
            if (target.GetComponent<Unit>() != null && target.GetComponent<Unit>().currentHealth <= 0)
                return false;
            float distance = Vector3.Distance(transform.position, target.position);
            return distance < rangedRange;
        }
        [HideInInspector]
        public bool lockRotation = false;
        private void RotateTowards(Transform target)
        {
            if (lockRotation) return;
            Vector3 direction = (target.position - transform.position).normalized; 

            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * agent.angularSpeed/5 );
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
}
