﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelPiker : MonoBehaviour
{
    //level description to operate;
    public StageSelector stageSelector;
    public LevelDescriptor ld;
    public Text text;
    public MainMenuLevelSelector mps;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMouseDown()
    {
        mps.gameObject.SetActive(false);
        stageSelector.ld = ld;
        stageSelector.gameObject.SetActive(true);

    }

}
