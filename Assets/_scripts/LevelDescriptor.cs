﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDescriptor 
{
    public int levelId;
    public string levelName;
    public string progressTrackValue;
    public class Stage
    {
        public string name;
        public int vaweId;
        public Stage (string v1, int v2)
        {
            name = v1;
            vaweId = v2;
        }
    }
    //todo remove when more than one levels will be
    public List<Stage> savePoints = new List<Stage>();

    public LevelDescriptor ()
    {
        levelId = 1;
        progressTrackValue = PlayerState.heaven_vaweId_name;
        levelName = "parasha govna";
        savePoints.Add(new Stage ("Cursed", 0));
        //blind
        savePoints.Add(new Stage("Blind", 5));
        //raven
        savePoints.Add(new Stage("Raven", 15));
        //metal
        savePoints.Add(new Stage("Metal", 25));
    }

    public bool contains (int v)
    {
        foreach (Stage st in savePoints)
        {
            if (st.vaweId.Equals(v))
                return true;
        }
        return false;
    }

}
