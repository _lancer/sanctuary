﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {


    public GameObject container;
    public GameObject prototype;
	public List <GameObject> pool = new List <GameObject> ();

	public int limit = -1;

	public GameObject Get (Vector3 where_ ) {
		return Get (where_, prototype.transform.rotation);
	}
	public GameObject Get (Vector3 where_, Quaternion rotation) {
		foreach (GameObject obj in pool) {
			if (!obj.activeSelf) {
				obj.transform.position = where_;
				obj.transform.rotation = rotation;
				obj.SetActive (true);
				return obj;
			}
		}
		if (limit != -1 && pool.Count >= limit)
			return null;
		GameObject go = GameObject.Instantiate (prototype, where_, rotation);
        if (container != null)
        {
            go.transform.SetParent(container.transform);
            go.transform.position = where_;
        }
		pool.Add (go);
		return go;
	}


}