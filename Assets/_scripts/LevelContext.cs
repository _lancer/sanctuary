﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelContext : MonoBehaviour
{
    public static LevelContext inst;
    public GameObject player;

    public ObjectPool splatPool;

    public ObjectPool cursedPool;

    public ObjectPool blindPool;
    public ObjectPool ravenPool;

    public ObjectPool metalPool;

    public ObjectPool tabletsPool;

    public AudioClip[] swings;
    public AudioClip[] smashes;


    // Start is called before the first frame update
    void Start()
    {
        inst = this;
        Invoke("InitPlayerStats", .3f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InitPlayerStats()
    {
        Debug.Log(PlayerState.Instance.SwordLevel);
        player.GetComponent<Unit>().blade.damage.amount = 35 + (PlayerState.Instance.SwordLevel - 1) * 3; 
        player.GetComponent<Unit>().armor.absolute = 5 + (PlayerState.Instance.ArmorLevel - 1) * 2;
        //blessing is Heal script in the button. who cares
    }

    public void pause ()
    {
        Time.timeScale = 0;
    }

    public void unpause ()
    {
        Time.timeScale = 1;
    }
}
