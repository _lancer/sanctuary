﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class Unit : MonoBehaviour
{
    // change to items
    public GameObject castEffect;
    public GameObject holyBuff;

    public float manaRevord;
    public float health;
    public float mana;
    //  [HideInInspector]
    public float currentHealth;
    public float currentMana;

    public Blade blade;

    public bool redblood = false;
    public int team = 2;
    public class Damage
    {
        public string type;
        public float amount;
    }
    public int _debugArmorPerc;
    public int _debugArmorAbs;
    public class Armor
    {
        public int percent;
        public int absolute;
    }
   public Armor armor;
    public void OnEnable()
    {
        if (blade != null)
            blade.enabled = true;
        currentHealth = health;
        currentMana = mana;
        if (GetComponent<NavMeshAgent>()!=null)
            GetComponent<NavMeshAgent>().enabled = true;
        GetComponent<Collider>().enabled = true;
        if (GetComponent<AICharacterControl>() != null)
            GetComponent<AICharacterControl>().enabled = true; 
        foreach (Rigidbody rb in reenable)
        {
            rb.gameObject.SetActive(true);
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            rb.isKinematic = true;
            rb.useGravity = false;
        }
        GetComponent<Animator>().enabled = true;
    }
    private void OnDisable()
    {
        foreach (Rigidbody rb in reenable)
        {
            rb.velocity = Vector3.zero; rb.useGravity = false; 
            rb.Sleep();
            rb.gameObject.SetActive(false);
        }
    }
    List<Rigidbody> reenable = new List<Rigidbody>();
    // Start is called before the first frame update
    
    public void Start()
    {
        armor = new Armor();
        armor.absolute = _debugArmorAbs;
        armor.percent = _debugArmorPerc;
        foreach (Rigidbody r in GetComponentsInChildren<Rigidbody>())
        {
            if (r.gameObject == this.gameObject) continue;
             r.velocity = Vector3.zero; r.useGravity = false;
            r.Sleep();
             
            reenable.Add(r);
        } 
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void heal (float amount)
    {
        currentHealth += amount;
        if (currentHealth > health)
            currentHealth = health;
    }

    static float percentage(int armor)
    {
        int val = 0;
        for (int i = 0; i < armor; i++)
        {
            int incr = 10 - i;
            if (incr < 1)
                incr = 1;
            val += incr;
        }
        return (float)val / 100;
    }
    bool slowedrecently = false;
    public void pauseAnimatorOnHit()
    {
        if (slowedrecently) return;
        slowedrecently = true;

        GetComponent<ThirdPersonCharacter>().m_AnimSpeedMultiplier = 0.2f;
        GetComponent<Animator>().speed = 0.2f; 
        Invoke("unause", .1f);
        Invoke("unause2", .15f);
        Invoke("unause3", .2f);
        Invoke("alowslow", .25f);
    }
    void unause()
    {
        GetComponent<ThirdPersonCharacter>().m_AnimSpeedMultiplier =.5f;
        GetComponent<Animator>().speed =.5f;
        
    }
    void unause2()
    {
        GetComponent<ThirdPersonCharacter>().m_AnimSpeedMultiplier = .8f;
        GetComponent<Animator>().speed = .8f;

    }
    void unause3()
    {
        GetComponent<ThirdPersonCharacter>().m_AnimSpeedMultiplier = 1f;
        GetComponent<Animator>().speed = 1f;

    }
    void alowslow()
    { 
        slowedrecently = false;
    }
    public static void ProcessDamage (Unit reciever, Damage damage, Vector3 direction, Unit dealer )
    {
        dealer.pauseAnimatorOnHit();
        dealer.gameObject.GetComponentInChildren<AudioSource>().Stop();
        dealer.gameObject.GetComponentInChildren<AudioSource>().clip = LevelContext.inst.smashes[Random.Range(0, LevelContext.inst.smashes.Length)];
        dealer.gameObject.GetComponentInChildren<AudioSource>().Play();

        float _damage = damage.amount - damage.amount * percentage(reciever.armor.percent);
        _damage -= reciever.armor.absolute; 
        if (_damage < .2f * damage.amount)
            _damage = .2f * damage.amount;
        //Debug.Log(_damage);
        reciever.currentHealth -= _damage;
        if (reciever.currentHealth <= 0)
            reciever.currentHealth = 0;
        if (!reciever.redblood)
        {
            Vector3 vec = reciever.transform.position;
            vec.y = 1.1f;
            GameObject go = LevelContext.inst.splatPool.Get(vec + Random.insideUnitSphere * .3f, Random.rotation);
            go.GetComponent<ParticleSystem>().Play(true);
        }
        if (damage.amount > reciever.health *.2f)
            reciever.GetComponent<Animator>().SetTrigger("Pain");
        if (reciever.currentHealth <= 0)
        {
            dealer.currentMana += reciever.manaRevord;
            if (dealer.currentMana > dealer.mana)
                dealer.currentMana = dealer.mana;
            reciever.Die(direction);

        }
      //  go.transform.position = reciever.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips).position;
    }
    Rigidbody rb;
    Vector3 direction;
    
    public virtual void Die (Vector3 direction)
    {
        // meh
        Spawner.killedCount++;
        if (blade!=null && blade.ps != null)
            blade.ps.Stop(false, ParticleSystemStopBehavior.StopEmitting);
        if (gameObject == LevelContext.inst.player)
            LevelContext.inst.gameObject.GetComponent<Spawner>().stop();
        if (blade != null)
            blade.enabled = false;
        if ( GetComponent<NavMeshAgent>() != null)
             GetComponent<NavMeshAgent>().enabled = false;
        GetComponent<Collider>().enabled = false;

        if (GetComponent<AICharacterControl>() != null)
            GetComponent<AICharacterControl>().enabled = false;
        if (GetComponent<ThirdPersonUserControl>() != null)
            GetComponent<ThirdPersonUserControl>().enabled = false;
        rb = GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips).GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.isKinematic = false;
        rb.useGravity = true;
        GetComponent<Animator>().enabled = false;
        foreach (Rigidbody rb in reenable)
        {

            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            rb.isKinematic = false;
            rb.useGravity = true;
            rb.WakeUp();
             
        }
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        Invoke("addForce", 0.15f);
        this.direction = direction;


    }

    void addForce ()
    {
        rb.AddForce((direction + Vector3.up*.5f) *  5020);
    }
     
}
