﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Cursed : Unit
{
    public GameObject DeathEffect;
    public GameObject DissolveEffect;
    public List<GameObject> disable;

    public float hitCD = 1.5f;
    float currentHtCd;
     new void OnEnable()
    {
        GetComponent<AudioSource>().mute = true;
        base.OnEnable();
        currentHtCd = hitCD;
        DissolveEffect.SetActive(false);
        DeathEffect.SetActive(false);
        disable.ForEach(d => d.SetActive(true));
    }
   public void Update()
    {
        if (currentHealth <= 0) return;

        if (GetComponent<AICharacterControl>().IsInMeleeRangeOf())
        {
            if (currentHtCd <= 0)
            {
                GetComponent<Animator>().SetTrigger("Hit");
                currentHtCd = hitCD;
            }
            else
                currentHtCd -= Time.deltaTime;
        }
        else
        {
            currentHtCd = hitCD;
        }
    }
    public override void Die(Vector3 direction)
    {
        base.Die(direction);
        Invoke("Drop", 5f);
        Invoke("Effects", 3f);
         
        //    Invoke("Effects", .3f);
    }
    static int killed = 0;
    private void Drop()
    {
        killed++;
        if (killed % 5 != 0) return;
        Rigidbody rigidBody = GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips).GetComponent<Rigidbody>();
        Vector3 vector = new Vector3(rigidBody.position.x, rigidBody.position.y + 0.5f, rigidBody.position.z);
        LevelContext.inst.tabletsPool.Get(vector);
    }

    void Effects()
    {
        DeathEffect.SetActive(true);

        Invoke("Dissolve", 3f);
        //  Invoke("Dissolve", .3f);
    }

    void Dissolve()
    {
        disable.ForEach(d => d.SetActive(false));
        DeathEffect.GetComponent<ParticleSystem>().Stop(false, ParticleSystemStopBehavior.StopEmitting);
        DissolveEffect.SetActive(true);
        Invoke("Disable", 1.7f);
        //   Invoke("Disable", .7f);
    }

    void Disable()
    {
        gameObject.SetActive(false);
    }

}
