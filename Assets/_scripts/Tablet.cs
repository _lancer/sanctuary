﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Tablet : MonoBehaviour
{
    bool move = false;
    public GameObject light;
    private void OnEnable()
    {
        move = false;
        light.SetActive(true);
        transform.localPosition = Vector3.zero;
    }
    private void Update()
    {
        if (move)
        {
            Vector3 moveTo = Camera.main.transform.position + Camera.main.transform.right * 2 + Vector3.up * 4;
            transform.position += (moveTo - transform.position).normalized * 15 * Time.deltaTime;
            if (Vector3.Distance (moveTo, transform.position) < .1f) 
                gameObject.transform.parent.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ThirdPersonUserControl>() == null)
            return;
        if (!other.GetComponent<ThirdPersonUserControl>().enabled)
            return;
        PlayerState.Instance.TabletsCount += 1;
        PlayerState.Instance.SavePrefs();
        move = true;
        GetComponent<AudioSource>().Play();
        light.SetActive(false);
    }
}
