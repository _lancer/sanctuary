﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentMenu : MonoBehaviour
{
    public GameObject TabletsCounter;
    public GameObject SwordLevelCounter;
    public GameObject SwordLevelUpgrade;
    public GameObject ArmorLevelCounter;
    public GameObject ArmorLevelUpgrade;
    public GameObject BlessingLevelCounter;
    public GameObject BlessingLevelUpgrade;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UpdateView();
    }

    public void UpgradeSword()
    {
        Upgrade(ref PlayerState.Instance.SwordLevel, ref PlayerState.Instance.TabletsCount);
    }

    public void UpgradeArmor()
    {
        Upgrade(ref PlayerState.Instance.ArmorLevel, ref PlayerState.Instance.TabletsCount);

    }

    public void UpgradeBlessing()
    {
        Upgrade(ref PlayerState.Instance.BlessingLevel, ref PlayerState.Instance.TabletsCount);

    }

    private void Upgrade(ref int level, ref int tabletsCount)
    {
        int tabletsCountAfterUpgrade = tabletsCount - GetNextLevelCost(level);
        if (tabletsCountAfterUpgrade >= 0)
        {
            level = level + 1;
            tabletsCount = tabletsCountAfterUpgrade;
        }
        PlayerState.Instance.SavePrefs();
    }

    private void UpdateView()
    {
        TabletsCounter.GetComponent<Text>().text = PlayerState.Instance.TabletsCount.ToString();
        SwordLevelCounter.GetComponent<Text>().text = PlayerState.Instance.SwordLevel.ToString();
        ArmorLevelCounter.GetComponent<Text>().text = PlayerState.Instance.ArmorLevel.ToString();
        BlessingLevelCounter.GetComponent<Text>().text = PlayerState.Instance.BlessingLevel.ToString();

        SwordLevelUpgrade.transform.GetChild(1).GetComponent<Text>().text = GetNextLevelCost(PlayerState.Instance.SwordLevel).ToString();
        ArmorLevelUpgrade.transform.GetChild(1).GetComponent<Text>().text = GetNextLevelCost(PlayerState.Instance.ArmorLevel).ToString();
        BlessingLevelUpgrade.transform.GetChild(1).GetComponent<Text>().text = GetNextLevelCost(PlayerState.Instance.BlessingLevel).ToString();

        CheckAvailability(PlayerState.Instance.SwordLevel, SwordLevelUpgrade.transform.GetChild(0).GetComponent<Image>());
        CheckAvailability(PlayerState.Instance.ArmorLevel, ArmorLevelUpgrade.transform.GetChild(0).GetComponent<Image>());
        CheckAvailability(PlayerState.Instance.BlessingLevel, BlessingLevelUpgrade.transform.GetChild(0).GetComponent<Image>());

    }

    private void CheckAvailability(int level, Image icon)
    {
        if (PlayerState.Instance.TabletsCount < GetNextLevelCost(level))
        {
            icon.color = new Color(87, 183, 229);
            icon.transform.parent.GetComponent<Button>().interactable = false;
        } else
        {
            icon.color = new Color(0, 200, 0);
            icon.transform.parent.GetComponent<Button>().interactable = true;
        }
    }

    private int GetNextLevelCost(int level) 
    {
        return (level + 1) * 10;
    }
}
