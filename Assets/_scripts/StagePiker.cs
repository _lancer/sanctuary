﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StagePiker : MonoBehaviour
{
    public StageSelector stageSelector;
    public LevelDescriptor ld;
    public Text text;
    public LevelDescriptor.Stage lds;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnMouseDown()
    {

        Spawner.waveId = lds.vaweId;
        SceneManager.LoadScene(ld.levelId);
    }
}
