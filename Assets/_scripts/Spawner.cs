﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Spawner : MonoBehaviour
{

    public GameObject SpawnPointsRoot;
    bool first = true;
    public static int waveId = 0;
    private int SpawnedCount;
    public static int killedCount;

    LevelDescriptor levelDesc = new LevelDescriptor();
    // Start is called before the first frame update
    void Start()
    {
    

        killedCount = 0;
        SpawnedCount = 0;
        InvokeRepeating("Spawn", .5f, 10);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void stop()
    {
        CancelInvoke("Spawn");
    }
    void Spawn ()
    {
        //postphone if too much on stage
        if (SpawnedCount - killedCount > 15)
            return;
        waveId++;
        //auto save
        if (levelDesc.contains(waveId))
        {
            if (PlayerState.Instance.heaven_vaweId < waveId)
            {
                PlayerPrefs.SetInt(PlayerState.heaven_vaweId_name, waveId);
                PlayerPrefs.Save();
                // update in runtime
                PlayerState.Instance.heaven_vaweId = waveId;
            }
        }
        int howmuch = 5;
        gos.Clear();
        for (int i = 0; i < howmuch; i++) {
            GameObject go = GetPool(i).Get(SpawnPointsRoot.transform.GetChild(Random.Range(0, SpawnPointsRoot.transform.childCount - 1)).transform.position);
            if (go == null)
                continue;
            go.GetComponent<AICharacterControl>().target = LevelContext.inst.player.transform;
            gos.Add(go);
            SpawnedCount++;
        }
        if (!first)
            Invoke ("DirtyFix", .7f);
    }
    List<GameObject> gos = new List<GameObject>();
    void DirtyFix( )
    {
        first = false;
        gos.ForEach(g => g.SetActive(false));
        Invoke("revoke", 7);
    }

    void revoke ( )
    { 
        gos.ForEach(g => g.SetActive(true));
    }
    private ObjectPool GetPool(int i)
    {
        if (waveId <=5) 
            return LevelContext.inst.cursedPool;
        else if (waveId <= 10)
        {
            if (i < 3)
                return LevelContext.inst.cursedPool;
            else
                return LevelContext.inst.blindPool;
        }
        else if (waveId <= 15)
        {
            if (i < 4)
                return LevelContext.inst.cursedPool;
            else
                return LevelContext.inst.blindPool;
        }
        else if (waveId <= 20)
        {

            if (i == 0)
                return LevelContext.inst.ravenPool;
            else if (i < 3)
                return LevelContext.inst.cursedPool;
            else
                return LevelContext.inst.blindPool;
        }
        else if(waveId <= 25)
            return LevelContext.inst.ravenPool;
        else if (waveId <= 30)
        {
            if (i == 0)
                return LevelContext.inst.metalPool;
            else
                return LevelContext.inst.ravenPool;
        }
        else if (waveId == 31)
        { 
            return LevelContext.inst.metalPool; 
        }
        //return boss;
        return null;
    }
    private ObjectPool GetPool()
    {
        int dice = Random.Range(0, 50) + System.Math.Min(SpawnedCount, 50);

        Debug.Log("Dice for spawner: " + dice);

        if (dice >= 95)
        {
            return LevelContext.inst.metalPool;
        } else if (dice >= 85)
        {
            return LevelContext.inst.blindPool;
        }
        return LevelContext.inst.cursedPool;
    }
}
