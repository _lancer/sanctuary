﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitAnimation : StateMachineBehaviour
{
    public float soundOffset = 0;
    public float hitOffset = 0;
    float currentTiming;
    float currentHitTiming;
    bool soundPlayed = false;
    bool hitactivated = false;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    { 
        
        currentTiming = soundOffset;
        currentHitTiming = hitOffset;
        soundPlayed = false;
        hitactivated = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!hitactivated)
        {
            currentHitTiming -= Time.deltaTime;
            if (currentHitTiming <= 0)
            {
                animator.gameObject.GetComponentInChildren<Blade>().Activate();
                hitactivated = true;
            }
        }
        if (!soundPlayed)
        {
            currentTiming -= Time.deltaTime;
            if (currentTiming <= 0)
            {
                animator.gameObject.GetComponentInChildren<AudioSource>().Stop();
                animator.gameObject.GetComponentInChildren<AudioSource>().clip = LevelContext.inst.swings[Random.Range(0, LevelContext.inst.swings.Length)];
                animator.gameObject.GetComponentInChildren<AudioSource>().Play();
                soundPlayed = true;
            }
        }

    }
 

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
