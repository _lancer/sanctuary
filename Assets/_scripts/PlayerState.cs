﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState
{
    public int TabletsCount;

    public int SwordLevel;
    public int ArmorLevel;
    public int BlessingLevel;

    //progress (autosaved in spawner)
    public int heaven_vaweId;
    public static string heaven_vaweId_name = "heaven_vaweId";


    private Dictionary<PlayerItems, Item> items;

    public static PlayerState Instance
    {
        set => instance = value;
        get
        {
            if (PlayerState.instance == null)
            {
                PlayerState.instance = new PlayerState();
            }

            return PlayerState.instance;
        }
    }
    private static PlayerState instance;

    public PlayerState()
    {
        TabletsCount = PlayerPrefs.GetInt(PlayerItems.Tablets.ToString(), 100);
        ArmorLevel = PlayerPrefs.GetInt(PlayerItems.LeatherArmor.ToString(), 1);
        SwordLevel = PlayerPrefs.GetInt(PlayerItems.Sword.ToString(), 1);
        BlessingLevel = PlayerPrefs.GetInt(PlayerItems.Blessing.ToString(), 1);

        //autosaved in spawner
        heaven_vaweId = PlayerPrefs.GetInt(heaven_vaweId_name, 0);

        items = new Dictionary<PlayerItems, Item>();

        items.Add(PlayerItems.Sword, new Item());
        items.Add(PlayerItems.LeatherArmor, new Item());
        items.Add(PlayerItems.SteelArmor, new Item());
        items.Add(PlayerItems.Bracer, new Item());
        items.Add(PlayerItems.Glow, new Item());
        items.Add(PlayerItems.Cross, new Item());
    }

    public int GetLevel(PlayerItems itemKey)
    {
        return items[itemKey].Level;
    }

    public bool IsUpgradeAvailable(PlayerItems itemKey)
    {
        Item item = items[itemKey];
        return TabletsCount >= item.Level * 10;
    }

    public void Upgrade(PlayerItems itemKey)
    {
        items[itemKey].Upgrade();
    }

    public void SavePrefs()
    {
        PlayerPrefs.SetInt(PlayerItems.Sword.ToString(), SwordLevel);
        PlayerPrefs.SetInt(PlayerItems.LeatherArmor.ToString(), ArmorLevel);
        PlayerPrefs.SetInt(PlayerItems.Blessing.ToString(), BlessingLevel);
        PlayerPrefs.SetInt(PlayerItems.Tablets.ToString(), TabletsCount);
       
        PlayerPrefs.Save();
    }

    public enum PlayerItems
    {
        Sword,
        LeatherArmor,
        SteelArmor,
        Bracer,
        Glow,
        Cross,
        Blessing,
        Tablets
    }

    private class Item
    {
        public int Level;

        public Item()
        {
            Level = 1;
        }

        public void Upgrade()
        {
            Level += 1;
        }
    }
}
