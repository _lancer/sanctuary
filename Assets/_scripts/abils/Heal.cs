﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Heal : MonoBehaviour
{
    public float cd = 2.5f;
    public float currentCD = 0;

    public Image cooldownUI;

    public AudioClip clip;

    private void Update()
    {
        if (currentCD > 0)
        {
            cooldownUI.fillAmount = 1- (currentCD/cd);
            currentCD -= Time.deltaTime;
        }
        else
            cooldownUI.fillAmount = 1;
    }
    public void execute (GameObject caster )
    {
        if (currentCD > 0) return;
        if (caster.GetComponent<Unit>().currentMana < 20) return;
        Camera.main.GetComponent<AudioSource>().clip = clip;
        Camera.main.GetComponent<AudioSource>().Play();
        currentCD = cd;
        caster.GetComponent<Unit>().currentMana -= 20;
        caster.GetComponent<Unit>().heal ( 20 + PlayerState.Instance.BlessingLevel - 1);
        caster.GetComponent<Animator>().SetTrigger("Cast");
        caster.GetComponent<Unit>().castEffect.SetActive(true);
        caster.GetComponent<Unit>().holyBuff.SetActive(true);
    }
}
