﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade : MonoBehaviour
{

    public List<Unit> triggered = new List<Unit>();
    public Unit.Damage damage;
    public float _damage;
    public bool active = false;
    public ParticleSystem ps;
    private void OnDisable()
    {
        triggered.Clear();
    }
    // Start is called before the first frame update
    void Start()
    {
        damage = new Unit.Damage();
        damage.amount = _damage;  
    }

   public float psPlaytime = 0;
    // Update is called once per frame
    void Update()
    {
        if (ps == null) return;
        if (psPlaytime < 0f)
        {
            ps.Stop(false, ParticleSystemStopBehavior.StopEmitting);//gameObject.SetActive(false);
            psPlaytime = 0;
        }
        else
        {
           
            psPlaytime -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!active) return;
        if (other.GetComponent<Unit>() == null) return;
        if (other.transform.root == transform.root) return;
        if (other.GetComponent<Unit>().team == transform.root.GetComponent<Unit>().team) return;
        if (triggered.Contains(other.GetComponent<Unit>())) return;
       
        triggered.Add(other.GetComponent<Unit>());
        if (ps == null) Unit.ProcessDamage(other.GetComponent<Unit>(), damage, transform.root.forward, transform.root.GetComponent<Unit>());
        else Unit.ProcessDamage(other.GetComponent<Unit>(), damage, ps.transform.up, transform.root.GetComponent <Unit>());
        if (ps == null) return;
        psPlaytime += .15f; 
        if (!ps.isEmitting)  ps.Play();// gameObject.SetActive(true);
    }

    public void Activate()
    {
        active = true;
        triggered.Clear();
    }
}
