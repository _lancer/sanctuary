﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessTerrainEmulator : MonoBehaviour
{
    float min = .025f;
    float max = .99f;
    float t = .5f;
    bool fadein = false;
    bool fadeout = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (fadein)
            RenderSettings.fogDensity = Mathf.Lerp(RenderSettings.fogDensity, max, t * Time.deltaTime);
        if (fadein && RenderSettings.fogDensity >= 0.9f)
        {
            fadein = false;
            Vector3 moveto = transform.parent.position;;
            moveto.y = LevelContext.inst.player.transform.position.y;

            Vector3 diff = moveto - LevelContext.inst.player.transform.position;
            //я сам охуел
            LevelContext.inst.cursedPool.pool.ForEach(x => x.transform.position += diff);
            LevelContext.inst.blindPool.pool.ForEach(x => x.transform.position += diff);
            LevelContext.inst.ravenPool.pool.ForEach(x => x.transform.position += diff);
            LevelContext.inst.metalPool.pool.ForEach(x => x.transform.position += diff);
            LevelContext.inst.player.transform.position += diff;
            fadeout = true;
        }
        if (fadeout)
            RenderSettings.fogDensity = Mathf.Lerp(RenderSettings.fogDensity, min, t * Time.deltaTime);
        if (fadeout && RenderSettings.fogDensity <= 0.03f)
        {
            fadeout = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == LevelContext.inst.player)
            _do ();
    }

    void _do()
    {
        fadein = true;
    }
}
