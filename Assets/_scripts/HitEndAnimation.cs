﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class HitEndAnimation : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        try
        {
            animator.gameObject.GetComponentInChildren<AudioSource>().Stop();
            animator.gameObject.GetComponentInChildren<Blade>().active = false;
        }
        catch (Exception e) { }
    }
}
