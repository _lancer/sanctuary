﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private int lastValue = 0;

    private List<string> queue;

    // Start is called before the first frame update
    void Start()
    {
        queue = new List<string>();
        gameObject.GetComponent<Text>().CrossFadeAlpha(0f, 0f, false);
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerState.Instance.TabletsCount != lastValue)
        {
            lastValue = PlayerState.Instance.TabletsCount;
            gameObject.GetComponent<Text>().CrossFadeAlpha(1f, 0.2f, false);
            queue.Add(null);
            Invoke("Hide", 2);
        }

        gameObject.GetComponent<Text>().text = PlayerState.Instance.TabletsCount.ToString();
    }

    void Hide()
    {
        if (queue.Count > 0)
        {
            queue.RemoveAt(queue.Count - 1);
        }

        if (queue.Count == 0)
        {
            gameObject.GetComponent<Text>().CrossFadeAlpha(0f, 0.5f, false);
        }
    }
}
