﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuLevelSelector : MonoBehaviour
{
    //list
    LevelDescriptor ld = new LevelDescriptor();
    public StageSelector stageSelector;

    public GameObject levelSelectorButton;
    public GameObject buttonsContainer;
    // Start is called before the first frame update
    void Start()
    {
        //foreach
        GameObject go = GameObject.Instantiate(levelSelectorButton);
        go.transform.parent = buttonsContainer.transform;
        go.GetComponent<LevelPiker>().stageSelector = stageSelector;
        go.SetActive(true);
        go.GetComponent<LevelPiker>().text.text = ld.levelName;
        go.GetComponent<LevelPiker>().ld = ld;
        go.GetComponent<LevelPiker>().mps = this;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
