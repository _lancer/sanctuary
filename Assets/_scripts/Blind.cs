﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Blind : Cursed
{
    public static ObjectPool pool;
    // Start is called before the first frame update
    void Awake ()
    {
        if (pool != null) return;
        GetComponent<Animator>().SetFloat("Attack Speed", .5f);
        GameObject container = new GameObject();
        container.AddComponent<ObjectPool>();
        pool = container.GetComponent<ObjectPool>();
        pool.prototype = Resources.Load<GameObject>("Spear (Throwable)");
    }
    new void Start()
    {
        base.Start();
    } 
    public float rangedCD;
      float currrangedCD;
    // Update is called once per frame
   new void Update()
    {
        base.Update();

        if (currentHealth <= 0) return;

        if (!GetComponent<AICharacterControl>().IsInMeleeRangeOf() && GetComponent<AICharacterControl>().IsInRangedRangeOf())
        {
            if (currrangedCD <= 0)
            {
                GetComponent<Throw>().throw_();
                currrangedCD = rangedCD;
            }
            else
                currrangedCD -= Time.deltaTime;
        }
        else
        {
            currrangedCD = rangedCD;
        }
    }

    
}
