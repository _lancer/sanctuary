﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSelector : MonoBehaviour
{
    public LevelDescriptor ld;

    public GameObject stageSelectorButton;
    public GameObject buttonsContainer;

    public List<GameObject> cleanup = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {

        foreach (GameObject but in cleanup)
            //garbage here
            GameObject.Destroy(but);
        cleanup.Clear();
        foreach (LevelDescriptor.Stage lds in ld.savePoints)
        {
            if (lds.vaweId > PlayerState.Instance.heaven_vaweId)
                break;
            GameObject go = GameObject.Instantiate(stageSelectorButton);

            go.transform.parent = buttonsContainer.transform;
            cleanup.Add(go);
            go.SetActive(true);
            go.GetComponent<StagePiker>().text.text = lds.name;
            go.GetComponent<StagePiker>().lds = lds;
            go.GetComponent<StagePiker>().ld = ld;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
