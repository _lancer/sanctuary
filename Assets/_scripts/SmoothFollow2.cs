﻿using UnityEngine;

namespace UnityStandardAssets.Utility
{
	public class SmoothFollow2 : MonoBehaviour
	{

        // The target we are following
        public Vector3  offset;
		[SerializeField] 
		public Transform target;
        // The target we are following
        [SerializeField]
        public Transform lookAt;
        [HideInInspector]
        Transform currentLookAt;

        // The distance in the x-z plane to the target
        [SerializeField]
		private float distance = 10.0f;
		// the height we want the camera to be above the target
		[SerializeField]
		private float height = 5.0f;

		[SerializeField]
		private float rotationDamping;
		[SerializeField]
		private float heightDamping;

		// Use this for initialization
		void Start() { currentLookAt = lookAt; }

        public Vector3 lookAtOffset;
		// Update is called once per frame
		void LateUpdate()
		{
			// Early out if we don't have a target
			if (!target)
				return;

            Vector3 toTarget = (lookAt.position - transform.position).normalized;

            if (Vector3.Dot(toTarget, transform.forward) < 0)
            {
                currentLookAt = target;
            }
            else
            {
                currentLookAt = lookAt;
            }
                 
			// Calculate the current rotation angles
			var wantedRotationAngle = target.eulerAngles.y;
			var wantedHeight = target.position.y + height;

			var currentRotationAngle = transform.eulerAngles.y;
			var currentHeight = transform.position.y;

			// Damp the rotation around the y-axis
			currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

			// Damp the height
			currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

			// Convert the angle into a rotation
			var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

			// Set the position of the camera on the x-z plane to:
			// distance meters behind the target
			transform.position = target.position;
			transform.position -= currentRotation * Vector3.forward * distance;

			// Set the height of the camera
			transform.position = new Vector3(transform.position.x ,currentHeight , transform.position.z);

            // Always look at the target
            offset.y = Vector3.Distance(target.position, currentLookAt.position)/3-3;

            Vector3 targetDir = currentLookAt.position - transform.position - offset + lookAtOffset;

            // The step size is equal to speed times frame time.
            float step = .1f * Time.deltaTime;

            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
         //   Debug.DrawRay(transform.position, newDir, Color.red);

            // Move our position a step closer to the target.
            transform.rotation = Quaternion.LookRotation(newDir);
        
           // transform.rotation = Quaternion.Lerp (transform.rotation, transform.LookAt(lookAt.position - offset);
		}
	}
}