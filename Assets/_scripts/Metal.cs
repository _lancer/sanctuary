﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Metal : Cursed
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Animator>().SetFloat("Movement Speed", .6f);
        armor = new Armor();
        armor.absolute = _debugArmorAbs;
        armor.percent = _debugArmorPerc;
    }
     
    public float hitCD = 3.5f;
    float currentHtCd;
    public void Update()
    {
        if (currentHealth <= 0) return;
        //задержка включения урона
        //выключение на обратном ходу
        //плавный поворот
        //пинок

        if (GetComponent<AICharacterControl>().IsInMeleeRangeOf() && currentHtCd <= 0)
        {
          
            GetComponent<Animator>().SetTrigger("Twohaaded");
            currentHtCd = hitCD;
            GetComponent<AICharacterControl>().lockRotation = true;
            
            Invoke("activateBlade", .5f);
            Invoke("deactivateBlade", 2);
            Invoke("unlockRotation", 3);
        }
        if (currentHtCd > 0)
        {
            currentHtCd -= Time.deltaTime;
        }
    }

    void unlockRotation() {
        GetComponent<AICharacterControl>().lockRotation = false;
    }
    void activateBlade()
    {
        GetComponentInChildren<Blade>().Activate();
    }
    void deactivateBlade()
    {
        GetComponentInChildren<Blade>().active = false ;
    }
}

