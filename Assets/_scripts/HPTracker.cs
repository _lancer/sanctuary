﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPTracker : MonoBehaviour
{

    public GameObject track; 
    public bool follow = true;

    public GameObject redBar;

    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (track == null)
            return;
        if (follow)
        transform.position = Camera.main.WorldToScreenPoint(track.transform.position + offset);
        redBar.transform.localScale = new Vector3(track.transform.root.GetComponent<Unit>().currentHealth / track.transform.root.GetComponent<Unit>().health, 1, 1);
    }
}
