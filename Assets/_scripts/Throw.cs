﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throw : MonoBehaviour
{
     
    public GameObject throwpoint;
    public GameObject itemInHand;

    float y = 2.2f;
    public float replacementDelay;

    public void throw_()
    {
        if (GetComponent<Unit>().currentHealth <= 0)
            return;
       // if (!itemInHand.activeSelf || itemInHand.GetComponent <SpearInHand>() .effect.isPlaying)
     //       return;
        GetComponent<Animator>().SetTrigger("Throw");
        Invoke("Replace", replacementDelay);
    }

    void Replace ()
    {
        itemInHand.SetActive(false);
        Vector3 pos = throwpoint.transform.position;
        pos.y = y;
        GameObject throwable = Blind.pool.Get(pos,  throwpoint.transform.rotation);
        
        throwable.GetComponent<Spear>().owner = gameObject;
       
        // throwable.GetComponent<Spear>().GetComponentInChildren<Collider>().enabled = true;
       
        throwable.GetComponent<Rigidbody>().AddForce((throwpoint.transform.forward ) * 850);
        Invoke("Revoke", 1.3f);

    }

    void Revoke ()
    {
        itemInHand.SetActive(true);
    }
}
