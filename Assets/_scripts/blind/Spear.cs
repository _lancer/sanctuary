﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spear : MonoBehaviour
{
    [HideInInspector]
    public GameObject owner;
    GameObject empty;
    Unit.Damage _damage = new Unit.Damage();
    // Start is called before the first frame update
    void Start()
    {
        _damage.amount = 30;
          empty = new GameObject();
    }
     
    public GameObject particles;

    private void OnEnable()
    {
        GetComponent<Spear>().GetComponentInChildren<Collider>().enabled = true;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
         
        foreach (Renderer r in disable)
            r.enabled = true;
    }
    public void dssolve ()
    {

        particles.GetComponent<ParticleSystem>().Play(true);
        foreach (Renderer r in disable)
            r.enabled = false;
        Invoke("finaly", 2);
    }
    public void finaly ()
    { 
        GetComponent<Rigidbody>().isKinematic = false ;
        transform.parent = null;
        empty.transform.parent = null;
        empty.transform.localScale = Vector3.one;
        empty.transform.rotation = Quaternion.identity;
        transform.localScale = Vector3.one;
        foreach (Renderer r in disable)
            r.enabled = true;
        gameObject.SetActive(false);
       

    }
   public  Renderer[] disable;
    // Update is called once per frame
    void Update()
    {
        
        if (!GetComponent<Rigidbody>().isKinematic &&  
            Vector3.Distance (Vector3.zero, GetComponent<Rigidbody>().velocity) > .1f)
            transform.up = GetComponent<Rigidbody>().velocity;
    }

    GameObject hitted;
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.root.gameObject == owner)
            return;
        if (other.transform.root.GetComponent<Spear>() != null)
            return;
        if (other.transform.root.GetComponent<Unit>() != null)
        {
            if (other.transform.root.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head) != other.transform &&
                other.transform.root.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips) != other.transform &&
                other.transform.root.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest) != other.transform)
                return;
        }
            hitted = other.gameObject;
       // Debug.Log(hitted);
        Invoke("Kinematic", .035f );
        GetComponent<Spear>().GetComponentInChildren<Collider>().enabled = false;

        if (other.transform.root.GetComponent<Unit>() != null)
            
            Unit.ProcessDamage(other.transform.root.GetComponent<Unit>(), _damage, transform.up, owner.GetComponent<Unit>());
    }

    void Kinematic ()
    {

        empty.transform.SetParent(hitted.transform);
        transform.SetParent(empty.transform);

        GetComponent<Rigidbody>().isKinematic = true;

        Invoke("dssolve", 2);
    }
}
